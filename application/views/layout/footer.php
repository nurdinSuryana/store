</div>

<aside id="sidebar-right" class="sidebar-right">
    <div class="nano">
        <div class="nano-content">
            <a href="#" class="mobile-close d-md-none">
                Collapse <i class="fas fa-chevron-right"></i>
            </a>

            <div class="sidebar-right-wrapper">

                <div class="sidebar-widget widget-calendar">
                    <h6>Upcoming Tasks</h6>
                    <div data-plugin-datepicker data-plugin-skin="dark"></div>

                    <ul>
                        <li>
                            <time datetime="2021-04-19T00:00+00:00">04/19/2021</time>
                            <span>Company Meeting</span>
                        </li>
                    </ul>
                </div>

                <div class="sidebar-widget widget-friends">
                    <h6>Friends</h6>
                    <ul>
                        <li class="status-online">
                            <figure class="profile-picture">
                                <img src="img/!sample-user.jpg" alt="Joseph Doe" class="rounded-circle">
                            </figure>
                            <div class="profile-info">
                                <span class="name">Joseph Doe Junior</span>
                                <span class="title">Hey, how are you?</span>
                            </div>
                        </li>
                        <li class="status-online">
                            <figure class="profile-picture">
                                <img src="img/!sample-user.jpg" alt="Joseph Doe" class="rounded-circle">
                            </figure>
                            <div class="profile-info">
                                <span class="name">Joseph Doe Junior</span>
                                <span class="title">Hey, how are you?</span>
                            </div>
                        </li>
                        <li class="status-offline">
                            <figure class="profile-picture">
                                <img src="img/!sample-user.jpg" alt="Joseph Doe" class="rounded-circle">
                            </figure>
                            <div class="profile-info">
                                <span class="name">Joseph Doe Junior</span>
                                <span class="title">Hey, how are you?</span>
                            </div>
                        </li>
                        <li class="status-offline">
                            <figure class="profile-picture">
                                <img src="img/!sample-user.jpg" alt="Joseph Doe" class="rounded-circle">
                            </figure>
                            <div class="profile-info">
                                <span class="name">Joseph Doe Junior</span>
                                <span class="title">Hey, how are you?</span>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</aside>

</section>

<!-- Vendor -->
<script src="<?=base_url('../asset/template/vendor/jquery/jquery.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jquery-browser-mobile/jquery.browser.mobile.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/popper/umd/popper.min.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/common/common.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/nanoscroller/nanoscroller.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/magnific-popup/jquery.magnific-popup.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jquery-placeholder/jquery.placeholder.js')?>"></script>

<!-- Specific Page Vendor -->
<script src="<?=base_url('../asset/template/vendor/jquery-ui/jquery-ui.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jqueryui-touch-punch/jquery.ui.touch-punch.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jquery-appear/jquery.appear.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/bootstrapv5-multiselect/js/bootstrap-multiselect.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jquery.easy-pie-chart/jquery.easypiechart.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/flot/jquery.flot.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/flot.tooltip/jquery.flot.tooltip.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/flot/jquery.flot.pie.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/flot/jquery.flot.categories.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/flot/jquery.flot.resize.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jquery-sparkline/jquery.sparkline.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/raphael/raphael.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/morris/morris.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/gauge/gauge.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/snap.svg/snap.svg.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/liquid-meter/liquid.meter.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jqvmap/jquery.vmap.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jqvmap/data/jquery.vmap.sampledata.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jqvmap/maps/jquery.vmap.world.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jqvmap/maps/continents/jquery.vmap.africa.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jqvmap/maps/continents/jquery.vmap.asia.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jqvmap/maps/continents/jquery.vmap.australia.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jqvmap/maps/continents/jquery.vmap.europe.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js')?>"></script>
<script src="<?=base_url('../asset/template/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js')?>"></script>

<!-- Theme Base, Components and Settings -->
<script src="<?=base_url('../asset/template/js/theme.js')?>"></script>

<!-- Theme Custom -->
<script src="<?=base_url('../asset/template/js/custom.js')?>"></script>

<!-- Theme Initialization Files -->
<script src="<?=base_url('../asset/template/js/theme.init.js')?>"></script>

<!-- Examples -->
<script src="<?=base_url('../asset/template/js/examples/examples.dashboard.js')?>"></script>

