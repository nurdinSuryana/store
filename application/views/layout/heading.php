<!doctype html>
<html class="has-tab-navigation header-dark">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Tab Navigation Layout | Porto Admin - Responsive HTML5 Template</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?=base_url('../asset/template/vendor/bootstrap/css/bootstrap.css')?>" />
		<link rel="stylesheet" href="<?=base_url('../asset/template/vendor/animate/animate.compat.css')?>">
		<link rel="stylesheet" href="<?=base_url('../asset/template/vendor/font-awesome/css/all.min.css')?>" />
		<link rel="stylesheet" href="<?=base_url('../asset/template/vendor/boxicons/css/boxicons.min.css')?>" />
		<link rel="stylesheet" href="<?=base_url('../asset/template/vendor/magnific-popup/magnific-popup.css')?>" />
		<link rel="stylesheet" href="<?=base_url('../asset/template/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css')?>" />
		<link rel="stylesheet" href="<?=base_url('../asset/template/vendor/jquery-ui/jquery-ui.css')?>" />
		<link rel="stylesheet" href="<?=base_url('../asset/template/vendor/jquery-ui/jquery-ui.theme.css')?>" />
		<link rel="stylesheet" href="<?=base_url('../asset/template/vendor/bootstrap-multiselect/css/bootstrap-multiselect.css')?>" />
		<link rel="stylesheet" href="<?=base_url('../asset/template/vendor/morris/morris.css')?>" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?=base_url('../asset/template/css/theme.css')?>" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?=base_url('../asset/template/css/skins/default.css')?>" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?=base_url('../asset/template/css/custom.css')?>">

		<!-- Head Libs -->
		<script src="<?=base_url('../asset/template/vendor/modernizr/modernizr.js')?>"></script>

	</head>
	<body>
		<section class="body">